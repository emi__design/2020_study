$(function() {
  $('[data-js="header"]').load("/header.html");
});

$(window).on("load", function() {
  var Sp = 767,
    Tab = 959,
    WindowWidth = window.innerWidth;

  var dropdownTrigger = $('[data-js="dropdown-trigger"]'),
    dropdownTriggerSp = $('[data-js="dropdown-trigger-sp"]'),
    dropdownSecondary = $('[data-js="dropdown-secondary"]'),
    dropdownLine = $('[data-js="dropdown-line"]'),
    dropdownFirst = $('[data-js="dropdown-first"]');

  var SpToggle = function() {
    dropdownTriggerSp.off("click");
    dropdownTriggerSp.on("click", function() {
      dropdownFirst.stop().slideToggle();
      $(this)
        .next()
        .toggleClass("is-Hidden");
      $(this).toggleClass("is-Active");
    });
  };

  var TabToggle = function() {
    dropdownTrigger.off("click");
    dropdownTrigger.on("click", function() {
      dropdownSecondary.stop().slideToggle();
      $(this)
        .find(".GlobalNav__Link")
        .toggleClass("is-Active");
    });
  };

  var PcToggle = function() {
    dropdownTrigger.off("mouseenter mouseleave");
    dropdownTrigger.on({
      mouseenter: function() {
        $(this)
          .find(dropdownSecondary)
          .stop()
          .slideDown();
        $(this)
          .find(dropdownSecondary)
          .removeClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .addClass("is-Active");
      },
      mouseleave: function() {
        $(this)
          .find(dropdownSecondary)
          .stop()
          .slideUp();
        $(this)
          .find(dropdownSecondary)
          .addClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .removeClass("is-Active");
      }
    });
  };

  if (WindowWidth <= Sp) {
    //　SPの処理
    dropdownLine.addClass("is-Hidden");
    dropdownFirst.addClass("is-Hidden");
    dropdownTrigger.off("mouseenter mouseleave");
    SpToggle();
  } else if (WindowWidth <= Tab) {
    //　Tabの処理
    dropdownTrigger.off("mouseenter mouseleave");
    dropdownLine.removeClass("is-Hidden");
    dropdownFirst.removeClass("is-Hidden");
    TabToggle();
  } else {
    // PCの処理
    dropdownLine.removeClass("is-Hidden");
    dropdownTrigger.off("click");
    dropdownFirst.removeClass("is-Hidden");
    PcToggle();
  }

  $(window).on("resize", function() {
    var CurrentWindowWidth = window.innerWidth;
    if (CurrentWindowWidth <= Sp && WindowWidth > Sp) {
      //SPの処理
      dropdownSecondary.removeAttr("style");
      dropdownSecondary.addClass("is-Hidden");
      $(".GlobalNav__Link").removeClass("is-Active");
      dropdownFirst.addClass("is-Hidden");
      dropdownTriggerSp.removeClass("is-Active");
      dropdownTrigger.on("click");
      dropdownTrigger.off("mouseenter mouseleave");
      SpToggle();
    } else if (
      (CurrentWindowWidth > Sp && WindowWidth <= Sp) ||
      (CurrentWindowWidth <= Tab && WindowWidth > Tab)
    ) {
      //TABの処理
      dropdownFirst.removeAttr("style");
      dropdownFirst.removeClass("is-Hidden");
      dropdownTriggerSp.removeClass("is-Active");
      dropdownSecondary.addClass("is-Hidden");
      dropdownSecondary.removeAttr("style");
      dropdownLine.removeClass("is-Hidden");
      $(".GlobalNav__Link").removeClass("is-Active");
      dropdownTrigger.on("click");
      dropdownTrigger.off("mouseenter mouseleave");
      TabToggle();
    } else if (CurrentWindowWidth > Tab && WindowWidth <= Tab) {
      //PCの処理
      dropdownTrigger.off("click");
      dropdownTrigger.on("mouseenter mouseleave");
      PcToggle();
    }
    WindowWidth = CurrentWindowWidth;
  });
});
