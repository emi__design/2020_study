$(function() {
  var $AccordionContent = $('[data-js="accordion_content"]'),
    $AccordionTitleContainer = $('[data-js="accordion_title_container"]'),
    $AccordionTitle = $('[data-js="accordion_title"]');
  $AccordionContent.addClass("is-Hidden");
  $AccordionTitleContainer.click(function() {
    $AccordionTitle.not(this).removeClass("is-Open");
    $(this).toggleClass("is-Open");
    $(this)
      .next()
      .slideToggle(300)
      .toggleClass("is-Hidden");
  });
});
