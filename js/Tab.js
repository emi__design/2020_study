$(function() {
  var $TabMenuItem = $('[data-js="tab_menu_item"]'),
    $TabContent = $('[data-js="tab_content"]');
  $TabMenuItem.click(function() {
    var $TabMenuIndex = $TabMenuItem.index(this);
    $TabMenuItem.removeClass("is-Active");
    $(this).addClass("is-Active");
    $TabContent
      .removeClass("is-Show")
      .eq($TabMenuIndex)
      .addClass("is-Show");
  });
});
