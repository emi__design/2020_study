//npm install -D gulp プラグイン名
var gulp = require("gulp"),
  browserSync = require("browser-sync"),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer");

/* =========================================================
 SASS
========================================================= */
gulp.task("sass", function() {
  return gulp
    .src("./scss/**/*.scss")
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(
      autoprefixer({
        cascade: false
      })
    )
    .pipe(gulp.dest("./css"));
});

/* =========================================================
 BROWSER SYNC
========================================================= */

gulp.task("serve", function() {
  browserSync({
    server: {
      baseDir: "./"
    }
  });
});

gulp.task("reload", done => {
  browserSync.reload();
  done();
});

gulp.task("watch", function() {
  gulp.watch("./html/**/*.html", gulp.task("reload"));
  gulp.watch("./css/*.css", gulp.task("reload"));
  gulp.watch("./js/*.js", gulp.task("reload"));
  gulp.watch("./scss/**/*.scss", gulp.task("sass"));
});

gulp.task("default", gulp.series(gulp.parallel("watch", "serve")));
