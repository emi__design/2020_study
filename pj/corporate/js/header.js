$(function() {
  $('[data-js="header"]').load("/pj/corporate/common_html/header.html");
});

$(window).on("load", function() {
  var windowWidth = window.innerWidth;
  var dropdownTrigger = $('[data-js="dropdown-trigger"]'),
    dropdown = $('[data-js="grobal-nav-dropdown"]'),
    dropdownHidden = $('[data-js="grobal-nav-dropdown-none"]');

  if (windowWidth >= 960) {
    // PCの処理
    dropdownHidden.removeClass("is-Hidden");
    dropdownTrigger.off("click");
    $('[data-js="header_nav"]').css("display", "block");
    dropdownTrigger.on({
      mouseenter: function() {
        $(this)
          .find(dropdown)
          .stop()
          .slideDown();
        $(this)
          .find(dropdown)
          .removeClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .addClass("is-Active");
      },
      mouseleave: function() {
        $(this)
          .find(dropdown)
          .stop()
          .slideUp();
        $(this)
          .find(dropdown)
          .addClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .removeClass("is-Active");
      }
    });
  } else if (windowWidth >= 768) {
    // TABの処理
    dropdownTrigger.off("mouseenter mouseleave");
    dropdownHidden.removeClass("is-Hidden");
    $('[data-js="header_nav"]').css("display", "block");

    dropdownTrigger.on("click", function() {
      $(this)
        .find(dropdown)
        .stop()
        .slideToggle();
      $(this)
        .find(dropdown)
        .toggleClass("is-Hidden");
      $(this)
        .find(".GlobalNav__Link")
        .toggleClass("is-Active");
    });
  } else {
    // SPの処理
    dropdownTrigger.off("mouseenter mouseleave");
    dropdownHidden.addClass("is-Hidden");
    $('[data-js="header_nav"]').css("display", "none");

    $('[data-js="toggle_drawer"]').on("click", function() {
      $(this).toggleClass("is-Active");
      if ($(this).hasClass("is-Active")) {
        $('[data-js="header_nav"]')
          .stop()
          .slideDown();
      } else {
        $('[data-js="header_nav"]')
          .stop()
          .slideUp();
      }
      // return false;
    });
  }
});

$(window).on("resize", function() {
  var windowWidth = window.innerWidth;
  var dropdownTrigger = $('[data-js="dropdown-trigger"]'),
    dropdown = $('[data-js="grobal-nav-dropdown"]'),
    dropdownHidden = $('[data-js="grobal-nav-dropdown-none"]');

  if (windowWidth >= 960) {
    // PCの処理
    window.location = window.location;
    dropdownHidden.removeClass("is-Hidden");
    // dropdownTrigger.off("click");
    // $('[data-js="header_nav"]').css("display", "block");
    dropdownTrigger.on({
      mouseenter: function() {
        $(this)
          .find(dropdown)
          .stop()
          .slideDown();
        $(this)
          .find(dropdown)
          .removeClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .addClass("is-Active");
      },
      mouseleave: function() {
        $(this)
          .find(dropdown)
          .stop()
          .slideUp();
        $(this)
          .find(dropdown)
          .addClass("is-Hidden");
        $(this)
          .find(".GlobalNav__Link")
          .removeClass("is-Active");
      }
    });
  } else if (windowWidth >= 768) {
    // TABの処理
    window.location = window.location;
    // dropdownTrigger.off("mouseenter mouseleave");
    dropdownHidden.removeClass("is-Hidden");
    // $('[data-js="header_nav"]').css("display", "block");

    dropdownTrigger.on("click", function() {
      $(this)
        .find(dropdown)
        .stop()
        .slideToggle();
      $(this)
        .find(dropdown)
        .toggleClass("is-Hidden");
      $(this)
        .find(".GlobalNav__Link")
        .toggleClass("is-Active");
    });
  } else {
    // SPの処理
    window.location = window.location;
    // dropdownTrigger.off("mouseenter mouseleave");
    dropdownHidden.addClass("is-Hidden");
    // $('[data-js="header_nav"]').css("display", "none");

    $('[data-js="toggle_drawer"]').on("click", function() {
      $(this).toggleClass("is-Active");
      if ($(this).hasClass("is-Active")) {
        $('[data-js="header_nav"]')
          .stop()
          .slideDown();
      } else {
        $('[data-js="header_nav"]')
          .stop()
          .slideUp();
      }
      // return false;
    });
  }
});
